document.addEventListener('DOMContentLoaded', function(){ 
    // Can wait for all dom as light page.
    addNavIconListener();

}, false);

function addNavIconListener () {
    var navigator = document.getElementById("navigator");
    var navToggle = document.getElementById("nav_toggle");
    navToggle.addEventListener("click", function() {
        navigator.classList.toggle("slide-in-left");
    });
    // Not worth it to toggle the menu out on window resize.
};
